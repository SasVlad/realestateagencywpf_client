﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstateAgency.WPF.ViewModel.Interfaces
{
   public interface IDialogWindowService
    {
        void ShowWindow(object viewModel);
    }
}
