﻿using RealEstateAgency.WPF.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealEstateAgency.WPF.Model.Models;
using RealEstateAgency.WPF.Model.Models.ModelDTO;

namespace RealEstateAgency.WPF.Model.Services
{
    public class RealEstateClassService : IRealEstateClassService
    {
        public async Task<List<RealEstateClassDTO>> GetAllRealEstateClasses()
        {
            return (await new SendToServerService<List<RealEstateClassDTO>, object>().GetDataByJsonObjectAsync("RealEstate/GetAllClasses")).Result;
        }
    }
}
