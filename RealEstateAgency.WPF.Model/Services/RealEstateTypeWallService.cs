﻿using RealEstateAgency.WPF.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealEstateAgency.WPF.Model.Models;
using RealEstateAgency.WPF.Model.Models.ModelDTO;

namespace RealEstateAgency.WPF.Model.Services
{
    public class RealEstateTypeWallService : IRealEstateTypeWallService
    {
        public async Task<List<RealEstateTypeWallDTO>> GetAllRealEstateTypeWalls()
        {
            return (await new SendToServerService<List<RealEstateTypeWallDTO>, object>().GetDataByJsonObjectAsync("RealEstate/GetAllTypeWalls")).Result;
        }
    }
}
