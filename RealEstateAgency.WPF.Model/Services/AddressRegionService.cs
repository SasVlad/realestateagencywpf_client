﻿using RealEstateAgency.WPF.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealEstateAgency.WPF.Model.Models;
using RealEstateAgency.WPF.Model.Models.ModelDTO;

namespace RealEstateAgency.WPF.Model.Services
{
    public class AddressRegionService : IAddressRegionService
    {
        public async Task<List<AddressRegionDTO>> GetAllRegions()
        {
            return (await new SendToServerService<List<AddressRegionDTO>, object>().GetDataByJsonObjectAsync("Address/AllRegions")).Result;
        }
    }
}
