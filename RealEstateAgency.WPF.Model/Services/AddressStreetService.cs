﻿using RealEstateAgency.WPF.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealEstateAgency.WPF.Model.Models;
using RealEstateAgency.WPF.Model.Models.ModelDTO;

namespace RealEstateAgency.WPF.Model.Services
{
    public class AddressStreetService : IAddressStreetService
    {
        public async Task<List<AddressStreetDTO>> GetAllStreets()
        {
            return (await new SendToServerService<List<AddressStreetDTO>, object>().GetDataByJsonObjectAsync("Address/GetAllStreets")).Result;
        }
    }
}
