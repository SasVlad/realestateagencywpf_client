﻿using RealEstateAgency.WPF.Model.Models;
using RealEstateAgency.WPF.Model.Models.ModelDTO;
using RealEstateAgency.WPF.Model.Models.ModelFilters;
using RealEstateAgency.WPF.Model.Models.ModelViewDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstateAgency.WPF.Model.Interfaces
{
    public interface IUserService
    {
        Task<List<UserViewDTO>> GetAllUsers();
        Task<ResponsePackage<UserDTO>> UpdateUserRecord(UserDTO user);
        Task<List<UserViewDTO>> FilterUsersRecord(UserFilterModel userFilterModel);
    }
}
