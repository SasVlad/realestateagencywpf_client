﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstateAgency.WPF.Model.Models.ModelFilters
{
    public class EmployeePostFilterModel
    {
        public int? EmployeePostID { get; set; }
        public string EmployeePostName { get; set; }
        public double? EmployeePostSalary { get; set; }
    }
}
